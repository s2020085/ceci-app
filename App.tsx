import { NavigationContainer } from '@react-navigation/native';
import { StatusBar } from 'expo-status-bar';
import { SafeAreaProvider } from 'react-native-safe-area-context';
import { createStackNavigator } from '@react-navigation/stack';
import { useState } from 'react';

import { createBottomTabNavigator } from '@react-navigation/bottom-tabs';
import { MaterialIcons, Ionicons, Entypo, FontAwesome5 } from '@expo/vector-icons';
import SettingScreen from './src/screens/login/SettingScreen';
import ChatScreen from './src/screens/chat/ChatScreen';
import ClinicScreen from './src/screens/clinic/ClinicScreen';
import EatingScreen from './src/screens/eating/EatingScreen';

const Stack = createStackNavigator();
const BottomTab = createBottomTabNavigator();

function App() {
  const [user, setUser] = useState(null);

  function BottomTabNavigator() {
    return (<BottomTab.Navigator
          initialRouteName={user?'Chat':'Setting'}
      >
          <BottomTab.Screen 
              name='Chat'
              component={ChatScreen}
              options={{
                  headerTitle: 'Chat',
                  headerTitleAlign: 'left',
                  tabBarIcon: ({color}) => <Entypo name="chat" size={28} color={color} />,
              }}
          />
          <BottomTab.Screen 
              name='Clinics'
              component={ClinicScreen}
              options={{
                  headerTitle: 'Clinics',
                  headerTitleAlign: 'left',
                  tabBarIcon: ({color}) => <FontAwesome5 name="clinic-medical" size={24} color={color} />,
              }}
          />
          <BottomTab.Screen 
              name='Eat'
              component={EatingScreen}
              options={{
                  headerTitle: 'Eating',
                  headerTitleAlign: 'left',
                  tabBarIcon: ({color}) => <MaterialIcons name="food-bank" size={36} color={color} />,
              }}
          />
          <BottomTab.Screen 
              name='Settings'
              component={SettingScreen}
              initialParams={ {  } }
              options={{
                  headerTitle: 'Settings',
                  headerTitleAlign: 'left',
                  tabBarIcon: ({color}) => <Ionicons name="settings-outline" size={30} color={color} />,
              }}
          />
      </BottomTab.Navigator>);
  };

  return (
    <SafeAreaProvider>
      <NavigationContainer>
        <BottomTabNavigator/>
      </NavigationContainer>
      <StatusBar style="auto" />
    </SafeAreaProvider>
  );
}

export default App;
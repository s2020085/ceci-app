export default class ListItem {
    name: string;
    location: string;
    coordinates: Array<number>;
    constructor (name: string, location: string, coordinates: Array<number>) {
        this.name = name;
        this.location = location;
        this.coordinates = coordinates;
    }
}
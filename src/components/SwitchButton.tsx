import React from "react";
import { View, TouchableOpacity, Text } from "react-native";

export default function SwitchButton ({navigation, number, name, chosen, setChosen}: {navigation: any, number: number, name: string, chosen: string, setChosen: Function}) {
  const width = (100/number).toString() + "%";
  return (
    <TouchableOpacity style={ {width: width, height: "100%", justifyContent: "center", backgroundColor: (name==chosen)?"#aaa":"white"} } onPress={()=>{setChosen(name);navigation.navigate(name+"Chat", {navigation, name})}}>
      <Text style={ {textAlign: 'center', fontSize: 24} }>{name}</Text>
    </TouchableOpacity>
    );
  }
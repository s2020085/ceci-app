import { View, TextInput } from "react-native";
import { Dimensions } from "react-native";

export default function SearchBar ( {txt, changeFunc}: {txt: string, changeFunc: any} ) {
    const currentWindow = Dimensions.get('window');
    const {height, width, fontScale} = currentWindow;
    return (
        <View>
            <TextInput
            autoCapitalize="none"
            style={ {height: height*0.04, margin: width*0.01, padding: fontScale * 10, borderWidth: 1} }
            value={txt}
            onChangeText={changeFunc}
            autoFocus={true}
            />
        </View>
    );
}
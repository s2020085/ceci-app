import { View, Image, Text } from "react-native";
import { Dimensions } from "react-native";
import ListItem from "../class/ListItem";
import ViewListItem from "./ViewListItem";

export default function ViewList( {navigation, list, filter}: {navigation: any, list: Array<ListItem>, filter: any} ) {
    const itemPerScreen = 7;
    const height = Dimensions.get("window").height;

    const renderList = [];

    for (var i=0;i<list.length;++i) {
        const item = list[i];
        if (item.name.toLowerCase().includes(filter.toLowerCase()) || item.location.toLowerCase().includes(filter.toLowerCase()))
            renderList.push(
                <View key={i}>
                    <ViewListItem navigation={navigation} totalHeight={height/itemPerScreen} item={item} />
                </View>
            );
    }

    return (
        <View style={ {} }>
            {
                renderList
            }
        </View>
    );
}
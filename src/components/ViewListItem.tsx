import { View, Image, Text, StyleSheet } from "react-native";
import ListItem from "../class/ListItem";
import { Dimensions } from "react-native";
import { TouchableHighlight } from "react-native-gesture-handler";

export default function ViewListItem ( {navigation, item, totalHeight}: {navigation: any, item: ListItem, totalHeight: number} ) {
    const imageScale = 0.6;
    const {fontScale} = Dimensions.get("window");

    return (
        <TouchableHighlight onPress={()=>navigation.navigate('Details', item)}>
            <View style={ {...styles.container, height: totalHeight} }>
                {/* <View> 
                <Image
                source={ {uri: item.image} }
                style={ {width: totalHeight * imageScale, height: totalHeight * imageScale, marginRight: totalHeight * imageScale / 8, marginLeft: totalHeight * imageScale / 8, borderRadius: totalHeight / 2} }
                />
                </View>*/}
                <View style={ {flex: 1} }>
                <Text style={ {fontWeight: "bold", fontSize: 18 * fontScale, textAlign: 'center', marginBottom: '1%'} }>{item.name}</Text>
                <Text style={ {fontSize: 16 * fontScale, color: "#666"} }>{item.location}</Text>
                </View>
            </View>
        </TouchableHighlight>
        );
    }
    
    const styles = StyleSheet.create({
        container: {
            display: 'flex',
            flexDirection: 'row',
            alignItems: 'center',
            backgroundColor: '#fff',
            borderBottomWidth: 1,
            borderBottomColor: '#ddd',
        },
        textContainer: {
            flex: 1,
        },
    });
import { View } from "react-native";
import SwitchButton from "./SwitchButton";

export default function SwitchButtonList ({navigation, rooms, currentRoom, setCurrentRoom}: {navigation: any, rooms: Array<string>, currentRoom: string, setCurrentRoom: Function}) {
  
  const rms = [];

  for (var i=0;i<rooms.length;++i) {
    rms.push(
      <SwitchButton key={i} navigation={navigation} number={rooms.length} name={rooms[i]} chosen={currentRoom} setChosen={setCurrentRoom} />
    )
  }
  
  return (
    <View style={ {flexDirection: "row", height: "10%"} }>
      {rms}
    </View>
    );
  }
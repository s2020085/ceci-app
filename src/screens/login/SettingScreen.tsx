import React, { useState } from 'react'
import { createStackNavigator } from '@react-navigation/stack';
import { View, Text, StyleSheet, Button } from 'react-native';

import LoginPage from './LoginScreen';
import SignUpPage from './SignUpScreen'
import { storeData, getData, deleteData } from '../../function/data';

type ProfileProps = {
  username: string;
  email: string;
  password: string;
};

const Stack = createStackNavigator();

export default function SettingScreen({navigation}: {navigation: any}) {
  const [logined, setLogined] = useState(null);
  const [rawUserData, setRawUserData] = useState(null);
  getData("logined").then(state => setLogined(state));
  getData("userData").then(data => {setRawUserData(data)});

  const handleLogOut = () => {
    deleteData("logined");
    deleteData("userData");
    navigation.navigate("Login");
  }

  function SettingPage () {
    const render = [];
    
    const userData = JSON.parse(rawUserData);

    console.log(userData);

    const maskedPassword = userData.password.replace(/./g, '*');

    for (var key in userData) {
      render.push(<Text key={key}>{key}: {userData[key]}</Text>)
    }

    return (
      <View style={styles.container}>
        <Text style={styles.label}>Username:</Text>
        <Text style={styles.value}>{userData.fullName}</Text>
        <Text style={styles.label}>Email:</Text>
        <Text style={styles.value}>{userData.email}</Text>
        <Text style={styles.label}>Password:</Text>
        <Text style={styles.value}>{maskedPassword}</Text>
        <Button title="Log out" onPress={handleLogOut} />
      </View>
    );
  }

  return (
      <Stack.Navigator initialRouteName={(logined=="success")?"Setting":"Login"}>
        <Stack.Screen name="Setting" component={SettingPage} />
        <Stack.Screen name="Login" component={LoginPage} />
        <Stack.Screen name="SignUp" component={SignUpPage} />
      </Stack.Navigator>
  );
}


const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 16,
  },
  label: {
    fontSize: 16,
    fontWeight: 'bold',
    marginTop: 8,
  },
  value: {
    fontSize: 16,
    marginTop: 4,
  },
});
import React, { useEffect } from "react";
import { View, ScrollView, Text } from "react-native";
import ViewList from "../../components/ViewList";
import SearchBar from "../../components/SearchBar";
import { createStackNavigator } from "@react-navigation/stack";
import * as Location from 'expo-location';
import DetailScreen from "../DetailScreen";
import MapView from "react-native-maps";
import ListItem from "../../class/ListItem";

const Stack = createStackNavigator();

async function getNearbyHealthcare(search: string) {
    // Request permission to access location data
    let { status } = await Location.requestForegroundPermissionsAsync();
    if (status !== 'granted') {
        // Permission was not granted, handle it here
        return;
    }
    
    // Get the current location of the device
    let location = await Location.getCurrentPositionAsync({});
    let { latitude, longitude } = location.coords;

    // Set up the Geoapify Places API request
    let apiKey = '3b2a2307594b4feab34de9f4ad055334';
    let radius = 1000; // Search radius in meters
    let type = 'healthcare';
    let url = `https://api.geoapify.com/v2/places?categories=${type}&filter=circle:${longitude},${latitude},${radius}&apiKey=${apiKey}`;
    
    // Fetch data from the Geoapify Places API
    let response = await fetch(url);
    let data = await response.json();
    const healthcare = [];
    
    for (var i=0;i<data.features.length;++i) {
        let item = data.features[i];
        if (item.properties.name.includes(search) || item.properties.address_line2.includes(search)) {
            healthcare.push(new ListItem(item.properties.name, item.properties.address_line2, item.geometry.coordinates));
        }
    }
    // console.log(healthcare);
    return healthcare;
}

export default function ClinicScreen ( {navigation}: {navigation: any} ) {
    const [searchTxt, setSearchTxt] = React.useState("");
    const [healthcareList, setHealthcareList] = React.useState([]);

    useEffect(() => {
        getNearbyHealthcare(searchTxt).then((data) => setHealthcareList(data))
    }, [searchTxt]);

    function Main() {
        return (
            <View>
                <SearchBar txt={searchTxt} changeFunc={setSearchTxt} />
                {healthcareList.length?
                <ScrollView>
                    <ViewList navigation={navigation} list={healthcareList} filter={searchTxt} />
                </ScrollView>:
                <Text>Loading......</Text>
                }
            </View>  
        );
    }

    return (
        <Stack.Navigator initialRouteName="ClinicsMain">
            <Stack.Screen name="ClinicsMain" component={Main} options={ {headerTitle: "Clinics"} } />
            <Stack.Screen name="Details" component={DetailScreen}
            options={ {headerBackTitle: "Go Back"} }
            />
        </Stack.Navigator>
    );
}
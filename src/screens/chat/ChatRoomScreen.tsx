import React, { useState, useEffect } from 'react';
import { GiftedChat } from 'react-native-gifted-chat';
import 'firebase/auth';
import 'firebase/firestore';

import firebase from '../../firebase/config';
import { getData } from '../../function/data';

const auth = firebase.auth();
const db = firebase.firestore();

const ChatScreen = ( {navigation, roomName}: {navigation: any, roomName: string} ) => {
  const [user, setUser] = useState(null);
  getData("userData")
  .then((data) => setUser(JSON.parse(data)))
  .catch((error) => {console.log(error)});
  const [messages, setMessages] = useState([]);

  

  useEffect(() => {
    const messagesRef = db.collection('messages'+roomName.toLocaleUpperCase());
    const unsubscribeMessages = messagesRef
      .orderBy('createdAt', 'desc')
      .onSnapshot((querySnapshot) => {
        const newMessages = querySnapshot.docs.map((doc) => {
          const data = doc.data();
          return {
            _id: doc.id,
            text: data.text,
            createdAt: data.createdAt.toDate(),
            user: data.user,
          };
        });
        setMessages(newMessages);
      });

    return unsubscribeMessages;
  }, []);

  const onSend = async (newMessages = []) => {
    const message = newMessages[0];
    const messagesRef = db.collection('messages'+roomName.toLocaleUpperCase());
    await messagesRef.add({
      text: user?.fullName+"\n"+message.text,
      createdAt: new Date(),
      user: {
        _id: user?.id,
        name: user?.fullName,
        avatar: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSRn_WSkhlkvIDLdUgccUAidUEdvMTR-KXBxmw6Xaki&s",
      },
    });
  };
  
  return (
    <GiftedChat
      messages={messages}
      onSend={(user!==null)?onSend:()=>alert("Please login first!")}
      user={{
        _id: user?.uid,
        name: user?.fullName,
        avatar: "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSRn_WSkhlkvIDLdUgccUAidUEdvMTR-KXBxmw6Xaki&s",
      }}
    />
  );
};

export default ChatScreen;

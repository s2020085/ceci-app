import { createStackNavigator } from "@react-navigation/stack";
import React, { useCallback, useState } from "react";
import { View, StyleSheet, Text, SectionList, Button } from "react-native";
import { GiftedChat } from "react-native-gifted-chat";
import ListItem from "../../class/ListItem";
import ChatRoomScreen from "./ChatRoomScreen";
import SwitchButton from "../../components/SwitchButton";
import SwitchButtonList from "../../components/SwitchButtonList";

const Stack = createStackNavigator();

export default function ChatScreen( {navigation}: {navigation: any} )  {
  const [currentRoom, setCurrentRoom] = useState("General");

  const rooms = ["General", "Gourmet", "Medical", "Work"];

  const chatScreens = [];

  for (var i=0;i<rooms.length;++i) {
    chatScreens.push(<Stack.Screen key={i} name={`${rooms[i]}Chat`} component={ChatRoomScreen} options={{headerShown: false}} />)
  }

  return (
    <View>
      <SwitchButtonList navigation={navigation} rooms={rooms} currentRoom={currentRoom} setCurrentRoom={setCurrentRoom} />
      <View style={ {height: "90%"} }>
        <Stack.Navigator initialRouteName="GeneralChat">
          {chatScreens}
        </Stack.Navigator>
      </View>
    </View>
  );
}

const styles = StyleSheet.create({
  container1: {
    flexGrow: 1,
    backgroundColor: '#fff',
    alignItems: 'center',
    flexdirection: 'column'
  },
  topicContainer: {
    flexDirection: 'row',
    backgroundColor: '#fff'
  },
  picker: {
    height: 50,
    width: 200,
    backgroundColor: '#F2F2F2'
  },
  text: {
    fontSize: 25
  }
});

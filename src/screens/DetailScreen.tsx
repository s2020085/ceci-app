import * as Location from 'expo-location';
import { View, Text, Image, Dimensions} from "react-native";
import MapView from 'react-native-maps';

export default function DetailScreen ( {route}: {route: any} ) {
    const {name, image, location, coordinates} = route.params;
    const {width, height, fontScale} = Dimensions.get("window");

    const [longitude, latitude] = coordinates;
    
    return (
        <View style={ {} }>
            <View style={ {height: '30%'} }>
                <Text style={ {fontWeight: 'bold', textAlign: 'center', fontSize: 28 * fontScale, marginTop: "5%", marginBottom: "10%", flexWrap: 'wrap'} }>{name}</Text>
                <Text style={ {textAlign: 'center', fontSize: 24 * fontScale, flexWrap: 'wrap'} }>{location}</Text>
            </View>
            <MapView
            style={ {width: '100%', height: '70%'} }
            region={{
                latitude: latitude,
                longitude: longitude,
                latitudeDelta: 0.0005,
                longitudeDelta: 0.0005,
            }}
            zoomEnabled={true}
            />
        </View>
    );
}
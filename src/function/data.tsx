import AsyncStorage from '@react-native-async-storage/async-storage';

const storeData = async (key: any, value: any) => {
    try {
      await AsyncStorage.setItem(key, value);
    } catch (error) {
      alert(error);
    }
};

const getData = async (key: any) => {
    try {
      const data = await AsyncStorage.getItem(key);
      return data;
    } catch (error) {
        alert(error);
    }
}

const deleteData = async (key: any) => {
  try {
    await AsyncStorage.removeItem(key);
  } catch (error) {
    alert(error);
  }
}

export { storeData, getData, deleteData };
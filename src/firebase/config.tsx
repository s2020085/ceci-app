// v9 compat packages are API compatible with v8 code
import firebase from 'firebase/compat/app';
import 'firebase/compat/auth';
import 'firebase/compat/firestore';


const firebaseConfig = {
  apiKey: "AIzaSyAyIluBg9IIIZewt1DBqWPtLcu6rLkTkWY",
  authDomain: "never-gonna-give-you-up-23d0d.firebaseapp.com",
  databaseURL: "https://never-gonna-give-you-up-23d0d-default-rtdb.firebaseio.com",
  projectId: "never-gonna-give-you-up-23d0d",
  storageBucket: "never-gonna-give-you-up-23d0d.appspot.com",
  messagingSenderId: "891504801647",
  appId: "1:891504801647:web:1a528f9de3fb8d89051a9a",
  measurementId: "G-7P34RVBLYX"
};

if (!firebase.apps.length) {
    firebase.initializeApp(firebaseConfig);
}

export default firebase;